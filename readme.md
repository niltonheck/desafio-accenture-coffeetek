Desafio Accenture - CoffeeTEK
===

Aplicativo desenvolvido conforme telas apresentadas no desafio proposto pela Accenture para vaga em time de Mobility.

Release (.apk - debug): https://drive.google.com/file/d/1yUQM_V2tO5oea_yRMLFm-1_7kz6JOQ4L/view?usp=sharing

Ambiente de Desenvolvimento:
--

**Ionic:**

*    Ionic CLI                     : 5.2.3 (/home/niltonheck/.nvm/versions/node/v10.16.0/lib/node_modules/ionic)
*    Ionic Framework               : @ionic/angular 4.7.1
*    @angular-devkit/build-angular : 0.801.2
*    @angular-devkit/schematics    : 8.1.2
*    @angular/cli                  : 8.1.2
*    @ionic/angular-toolkit        : 2.0.0


**Cordova:**

*    Cordova CLI       : 9.0.0 (cordova-lib@9.0.1)
*    Cordova Platforms : android 8.0.0
*    Cordova Plugins   : cordova-plugin-ionic-keyboard 2.1.3, cordova-plugin-ionic-webview 4.1.1, (and 4 other plugins)


**Utility:**

*    cordova-res : 0.6.0
*    native-run  : 0.2.8

**System:**

*   Android SDK Tools : 26.1.1 (/home/niltonheck/Android/Sdk)
*    NodeJS            : v10.16.0 (/home/niltonheck/.nvm/versions/node/v10.16.0/bin/node)
*    npm               : 6.10.2
*    OS                : Linux 4.9
