import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { Product } from '../../models/Product';
import { Router } from '@angular/router';
import { NavigationService } from '../../services/navigation/navigation.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  products: Product[];

  constructor(
    private storeService: StoreService,
    private router: Router,
    private navigationService: NavigationService) {
    }

  ionViewWillEnter() {
    this.navigationService.setTitle('Menu');
    this.products = this.storeService.getProducts();
  }

  ngOnInit() {
  }

  onclick(product: Product): void {
    this.router.navigate([`/details/${product.id}`])
  }
}
