import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { Cart } from '../../models/Cart';
import { NavigationService } from '../../services/navigation/navigation.service';
import { CartItem } from '../../models/CartItem';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  cart: Cart = new Cart;

  constructor(private storeService: StoreService, private navigationService: NavigationService) {
  }

  ionViewWillEnter() {
    this.navigationService.setTitle('Carrinho');
    this.cart = this.storeService.getCart();

    this.cart.items.forEach(item => {
      item.extrasString = this.getExtras(item);
    });
  }

  getExtras(item: CartItem): string {
    if ( item.extras.length == 0 ) {
      return 'Sem adicionais.';
    }

    let extras: string[] = [];

    for (let i = 0; i < item.extras.length; i++) {
      const el = item.extras[i];
      extras.push(this.storeService.getExtra(el).name);
    }

    return extras.join(', ');
  }

  ngOnInit() {
  }
}
