import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { StoreService } from '../../services/store.service';
import { Product } from '../../models/Product';
import { CartItem } from '../../models/CartItem';
import { NavigationService } from '../../services/navigation/navigation.service';
import { Extra } from '../../models/Extra';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  actions: any;
  product: Product;
  item: CartItem = null;
  title: String;
  extras: Extra[];

  constructor(private router: Router, private route: ActivatedRoute, private storeService: StoreService, private navigationService: NavigationService) {
  }

  ionViewWillEnter() {
    this.navigationService.setTitle('Detalhes');

    /**
     * Neste caso, a navegação ocorre sempre da lista para o produto (ou na volta do carrinho).
     * Por isso, não usei observable. :-)
     */
    this.product = this.storeService.getProduct(this.route.snapshot.paramMap.get('id'));
    this.extras = this.storeService.getExtras();

    this.item = new CartItem();
    this.item.product = this.product;
  }

  ngOnInit() {
  }

  onaddtocart() {
    let uid = [this.item.product.id, this.item.size, this.item.sugar].join("");
    let uidext = this.item.extras.join("");
    let fuid = [uid, uidext].join("");

    this.item.uid = parseInt(fuid);

    this.storeService.addToCart(this.item);
    this.router.navigate([`/cart`])
  }

  onchangesize(size) {
    this.item.size = size;
  }

  onchangesugar(portion) {
    this.item.sugar = portion;
  }

  onchangequantity(quantity) {
    this.item.quantity += quantity;

    if ( this.item.quantity == 0 ) {
      this.item.quantity = 1;
    }
  }

  onchangeextra(extra) {
    let idx = null;

    if ( this.item.extras.includes(extra) ) {
      idx = this.item.extras.indexOf(extra);
      this.item.extras.splice(idx, 1);
    } else {
      this.item.extras.push(extra);
    }
  }

  getextrastyle(extra) {
    if ( !this.item.extras.includes(extra) ) {
      return {'opacity':'0.2'};
    }
  }

  getmeasure() {
    let measure = "";

    switch (this.item.size) {
      case 1:
        measure = '100 ML';
        break;

      case 2:
        measure = '200 ML';
        break;

      case 3:
        measure = '350 ML';
        break;
    
      default:
        break;
    }

    return measure;
  }

  getsizestyle(size) {
    if ( this.item.size != size ) {
      return {'opacity':'0.2'};
    }
  }

  getsugarstyle(portion) {
    if ( this.item.sugar != portion ) {
      return {'opacity':'0.2'};
    }
  }

  getquantitystyle(quantity) {
    if( this.item.quantity == 1 && quantity == -1 ) {
      return {'opacity':'0.2'};
    }
  }
}
