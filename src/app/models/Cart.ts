import { CartItem } from './CartItem';

export class Cart {
  items: CartItem[];

  constructor() {
    this.items = [];
  }
}