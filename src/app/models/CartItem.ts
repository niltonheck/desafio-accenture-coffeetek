import { Product } from './Product';

export class CartItem {
  product: Product;
  size: number = 1;
  quantity: number = 1;
  sugar: number = 0;
  extras: number[] = [];
  extrasString: string;
  uid: number;
}