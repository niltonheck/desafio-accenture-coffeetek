import { Extra } from './Extra';

export class Product {
  id: number;
  name: string;
  thumb: string;
}