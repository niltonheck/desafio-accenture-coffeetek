import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { CustomHeaderComponent } from './custom-header.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
  ],
  declarations: [CustomHeaderComponent],
  exports: [CustomHeaderComponent]
})
export class CustomHeaderComponentModule {}