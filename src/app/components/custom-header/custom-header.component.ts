import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { NavigationService } from '../../services/navigation/navigation.service';

@Component({
  selector: 'app-custom-header',
  templateUrl: './custom-header.component.html',
  styleUrls: ['./custom-header.component.scss'],
})
export class CustomHeaderComponent implements OnInit {
  title: string;
  showCart: boolean;
  showBack: boolean;

  constructor(private router: Router, private navigationService: NavigationService) {
    this.navigationService.stackobs.subscribe(stackConfig => {
      this.showBack = stackConfig[0];
      this.showCart = stackConfig[1];
    })

    this.navigationService.titleChanged.subscribe(newTitle => {
      this.title = newTitle;
    })
  }

  ngOnInit() {}

  onclickbackward() {
    this.router.navigate([this.navigationService.getPrevious()]);
  }

  onclickcart() {
    this.router.navigate(['/cart']);
  }
}
