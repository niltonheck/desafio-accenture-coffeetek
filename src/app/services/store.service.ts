import { Injectable } from '@angular/core';
import { Product } from '../models/Product';
import { Cart } from '../models/Cart';
import { CartItem } from '../models/CartItem';
import { Extra } from '../models/Extra';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private EXTRAS: Extra[] = [
    { id: 1, name: 'Chantily', thumb: 'cream.png' }
  ];

  private PRODUCTS: Product[] = [
    { id: 11, name: 'Espresso', thumb: 'Espresso.png' },
    { id: 12, name: 'Capuccino', thumb: 'Cappuccino.png' },
    { id: 13, name: 'Macchiato', thumb: 'macciato.png' },
    { id: 14, name: 'Mocha', thumb: 'Mocha.png' },
    { id: 15, name: 'Latte', thumb: 'latte.png' }
  ];

  private CART: Cart;

  constructor() {
    this.CART = new Cart();
  }

  getProducts(): Product[] {
    return this.PRODUCTS;
  }

  getExtras(): Extra[] {
    return this.EXTRAS;
  }

  getProduct(id): Product {
    return this.PRODUCTS.filter(product => product.id == id)[0];
  }

  getExtra(id): Extra {
    return this.EXTRAS.filter(extra => extra.id == id)[0];
  }

  getCart(): Cart {
    return this.CART;
  }

  clearCart(): void {
    this.CART = new Cart();
  }

  addToCart(_item: CartItem) {
    let item = Object.assign({}, _item);
    let isOnCart = false;
    let index = 0;

    for (let j = 0; j < this.CART.items.length; j++) {
      const element = this.CART.items[j];
      if ( element.uid == item.uid ) {
        isOnCart = true;
        index = j;
      };
    }

    if ( !isOnCart ) {
      this.CART.items.push(item);
    } else {
      this.CART.items[index].quantity += item.quantity;
    }
  }
}
