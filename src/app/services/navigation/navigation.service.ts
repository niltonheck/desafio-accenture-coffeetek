import { Injectable, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  private stack: String[] = [];
  public stackobs: Observable<any>;

  private disallowCart: String[] = ['/cart'];

  public titleChanged: EventEmitter<String>;

  private title: String;

  public titleObs: Observable<any>;

  constructor(private router: Router) {
    this.titleChanged = new EventEmitter();

    this.stack.push(this.router.url);

    this.stackobs = new Observable(obs => {
      this.router.events.subscribe((e) => {

        if(e instanceof NavigationEnd) {
          if(this.stack[this.stack.length - 1] != e.url && e.urlAfterRedirects == e.url) {
            this.stack.push(e.url);
          }

          if ( this.stack.length > 1 ) {
            obs.next([true, !this.disallowCart.includes(e.url)]);
          } else {
            obs.next([false, !this.disallowCart.includes(e.url)]);
          }
        }
      })
    });
  }

  setTitle(title: String) {
    this.titleChanged.emit(title);
  }

  getPrevious() {
    this.stack.pop();
    return this.stack[this.stack.length - 1];
  }

  hasPrevious() {
    return this.stack.length > 1;
  }
}
